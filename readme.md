Kolejny projekt, tym razem z wykorzystaniem Allegro 5.0.8.

Wymagania: MSVC++ 2010, ustawione �cie�ki do include'�w, lib'�w oraz dll'li od Allegro 5.0.8

********************************************************************

Changelog:

1)	*	pierwszy commit (fail)

2)	*	fix pierwszego commita

3)	*	usuni�cie ipch'a (precompiled headers�w) z katalogu projektu

4)	*	dodanie napisu ko�cowego: "koniec gry!" + "wygra�o k�ko" | "wygra� krzy�yk | remis"
	*	fix: sprawdzenie remisu
	*	dodanie restartu gry na klikni�cie prawym przyciskiem
	
5)	*	dodanie napisu "zaczyna krzyzyk | kolko"
	*	fix wysoko�ci napisu "wygra�o k�ko | wygra� krzy�yk"
	*	dodanie d�wi�ku, gdy kliknie si� w region ekranu, dla kt�rego nie ma �adnej akcji
	*	przesuni�cie ca�ego obszaru gry w prawo (dzi�ki fajnemu kodowi zmiana w jednej linijce �r�d�a + modyfikacja t�a)
	*	nowe grafiki: t�o (stare -> back_debug.png), krzy�yk, k�ko
	*	nowa czcionka (20 pkt), usuwanie z pami�ci czcionek po zako�czeniu programu
	*	przesuni�cie napis�w
	
6)	*	poprawny tytu� okna
	*	shutdown_font_addon, shutdown_ttf_addon w funkcji zwalniaj�cej zasoby
	*	restart dost�pny tylko po sko�czeniu gry
	*	fix wci�� (tabulator�w)
	*	polskie znaki (w ko�cu...) wystarczy�o zmieni� kodowanie pliku �r�d�owego na "UTF without signature"
	
7)	*	poprawione polskie znaki (main.c kodowany w UTF bez BOM, a consts.h w CP1250 :))

8)	*	nowe d�wi�ki
	*	b��dy nie pojawi� si� na strerr, tylko w message_box'ach
	*	critical fix, co� si� popieprzy�o ze sta�ymi x1, x2, x3... du�a refaktoryzacja sta�ych
	
9)	*	fix losowania, kto zaczyna