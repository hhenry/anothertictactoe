#ifndef GAME_LOGIC_C
#define GAME_LOGIC_C

#include "consts.h"
#include "game_logic.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>

short p[3][3];
short tura;


short prepare_structures(void)
{
	  short R; 
	  short L;

	  R = rand() % 10;
      if (R >= 5)
	  {
		  tura = GRACZ_KOLKO;
	  }
      else
	  {
		  tura = GRACZ_KRZYZ;
	  }
            
	  L = 9;
      while (L-- >= 0)
	  {
            p[L%3][L/3] = GRACZ_NIKT;
      }
	  stan = STAN_PRZED;
	  return 0;
}

short check_who_won(void)
{
	int tx = 0, ty = 0;

	/* SKOSNIE */
	if ((p[0][0] == p[1][1]) && (p[1][1] == p[2][2]) && (p[0][0] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[0][0] == p[1][1]) && (p[1][1] == p[2][2]) && (p[0][0] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	if ((p[2][0] == p[1][1]) && (p[1][1] == p[0][2]) && (p[0][2] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[2][0] == p[1][1]) && (p[1][1] == p[0][2]) && (p[0][2] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	/* PIONOWO */

	if ((p[0][0] == p[1][0]) && (p[1][0] == p[2][0]) && (p[0][0] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[0][0] == p[1][0]) && (p[1][0] == p[2][0]) && (p[0][0] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	if ((p[0][1] == p[1][1]) && (p[1][1] == p[2][1]) && (p[0][1] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[0][1] == p[1][1]) && (p[1][1] == p[2][1]) && (p[0][1] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	if ((p[0][2] == p[1][2]) && (p[1][2] == p[2][2]) && (p[0][2] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[0][2] == p[1][2]) && (p[1][2] == p[2][2]) && (p[0][2] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	/* POZIOMO */

	if ((p[0][0] == p[0][1]) && (p[0][1] == p[0][2]) && (p[0][0] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[0][0] == p[0][1]) && (p[0][1] == p[0][2]) && (p[0][0] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	if ((p[1][0] == p[1][1]) && (p[1][1] == p[1][2]) && (p[1][1] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[1][0] == p[1][1]) && (p[1][1] == p[1][2]) && (p[1][1] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	if ((p[2][0] == p[2][1]) && (p[2][1] == p[2][2]) && (p[2][0] == GRACZ_KOLKO))
			return GRACZ_KOLKO;
	if ((p[2][0] == p[2][1]) && (p[2][1] == p[2][2]) && (p[2][0] == GRACZ_KRZYZ))
			return GRACZ_KRZYZ;

	for (tx = 0; tx < 3; tx++)
	{
		for (ty = 0; ty < 3; ty++)
		{
			if (p[tx][ty] == GRACZ_NIKT)
				return GRACZ_NIKT;
		}
	}
	return GRACZ_REMIS;
}
short try_make_move(short X, short Y)
{
     if (p[X][Y] == GRACZ_NIKT)
	 {
        p[X][Y] = tura;                 
        if (tura == GRACZ_KOLKO)
		{
            tura = GRACZ_KRZYZ;           
        }
        else
            tura = GRACZ_KOLKO;
        return 0;
     }
     return -1;
}
short get_state(short X, short Y)
{
	if ((X >= 0) && (X <= 2))
		if ((Y >= 0) && ( Y <= 2))
			return p[X][Y];
	return -1;
}
#endif