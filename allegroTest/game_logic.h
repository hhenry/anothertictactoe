#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H

short stan;
short wygral;

short load_resources(void);
short prepare_structures(void);
short check_who_won(void);
short try_make_move(short X, short Y);
short get_state(short X, short Y);

#endif