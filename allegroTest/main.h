#ifndef MAIN_H
#define MAIN_H

#include "game_logic.h"

short do_beep(int which);
short unload_all(void);
short load_resources(void);
short destroy_resources(void);
short parse_mouse_input(int mX, int mY);

#endif
