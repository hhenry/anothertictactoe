#ifndef CONSTS_H
#define CONSTS_H

char *game_title;

const short STAN_PUSTY	= 0;
const short STAN_PRZED  = 1;
const short STAN_GRAMY	= 2;
const short STAN_KONIEC	= 4;

const short GRACZ_NIKT		= 0;
const short GRACZ_KOLKO		= 1;
const short GRACZ_KRZYZ		= 2;
const short GRACZ_KONIEC	= 3;
const short GRACZ_REMIS		= 4;

const short CONST_SPRITE_WIDTH		= 100;
const short CONST_SPRITE_HEIGHT		= 100;
const short CONST_SPRITE_START_X	= 300;
const short CONST_SPRITE_START_Y	= 100;

short _X1, _X2, _X3, _X4, _Y1, _Y2, _Y3, _Y4;

void prepare(void)
{
	_X1 = CONST_SPRITE_START_X;
	_X2 = _X1 + CONST_SPRITE_WIDTH;
	_X3 = _X2 + CONST_SPRITE_WIDTH;
	_X4 = _X3 + CONST_SPRITE_WIDTH;

	_Y1 = CONST_SPRITE_START_Y;
	_Y2 = _Y1 + CONST_SPRITE_HEIGHT;
	_Y3 = _Y2 + CONST_SPRITE_HEIGHT;
	_Y4 = _Y3 + CONST_SPRITE_HEIGHT;

	game_title = "K�ko i krzy�yk wykonane przez JL, 2013";
}
#endif