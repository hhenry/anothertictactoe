#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/utf8.h>
#include <allegro5/mouse.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_native_dialog.h>

#include "main.h"
#include "game_logic.c"

ALLEGRO_FONT		*FNT_20			= NULL;
ALLEGRO_FONT		*FNT_48			= NULL;

ALLEGRO_BITMAP		*BM_tlo			= NULL;
ALLEGRO_BITMAP		*BM_kolko		= NULL;
ALLEGRO_BITMAP		*BM_krzyz		= NULL;

ALLEGRO_SAMPLE		*SM_beep		= NULL;
ALLEGRO_SAMPLE_ID	SM_beep_ID;
ALLEGRO_SAMPLE		*SM_beep2		= NULL;
ALLEGRO_SAMPLE_ID	SM_beep2_ID;
ALLEGRO_SAMPLE		*SM_beep3		= NULL;
ALLEGRO_SAMPLE_ID	SM_beep3_ID;
ALLEGRO_SAMPLE		*SM_bg			= NULL;
ALLEGRO_SAMPLE_ID	SM_bg_ID;

ALLEGRO_EVENT		ev;
ALLEGRO_TIMER		*timer			= NULL;
ALLEGRO_DISPLAY		*display		= NULL;
ALLEGRO_EVENT_QUEUE *event_queue	= NULL;


short do_beep(int which)
{
	if (which == 1)
	{
		if (SM_beep != NULL)
		{
			al_stop_sample(&SM_beep_ID);
			al_play_sample(SM_beep, 1.0f, 1.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, &SM_beep_ID);
			return 0;
		}
	}
	if (which == 2)
	{
		if (SM_beep2 != NULL)
		{
			al_stop_sample(&SM_beep2_ID);
			al_play_sample(SM_beep2, 1.0f, 1.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, &SM_beep2_ID);
			return 0;
		}
	}
	if (which == 3)
	{
		if (SM_beep3 != NULL)
		{
			al_stop_sample(&SM_beep3_ID);
			al_play_sample(SM_beep3, 1.0f, 1.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, &SM_beep3_ID);
			return 0;
		}
	}
	return -1;
}
short load_resources(void)
{
	ALLEGRO_PATH *path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
	al_append_path_component(path, "assets");
	al_change_directory(al_path_cstr(path, '/'));
	al_destroy_path(path);

	BM_tlo		= al_load_bitmap("back.png"); 
	BM_kolko	= al_load_bitmap("dot.png"); 
	BM_krzyz	= al_load_bitmap("cro.png"); 

	al_reserve_samples(4);

	SM_beep		= al_load_sample("Abeep.wav"); 
	SM_beep2	= al_load_sample("Abeep2.wav"); 
	SM_beep3	= al_load_sample("Abeep3.wav");
	SM_bg		= al_load_sample("Abg.wav");

	FNT_48	= al_load_font("ubuntu.ttf", 48, 0);
	FNT_20	= al_load_font("ubuntu.ttf", 24, 0);

	if (BM_tlo == NULL)		return 1;
	if (BM_kolko == NULL)	return 1;
	if (BM_krzyz == NULL)	return 1;
	if (SM_bg == NULL)
	{
		al_show_native_message_box(display, "Błąd!", "Nie udało się wczytać pliku audio Abg.wav", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
	}
	if (SM_beep == NULL)
	{
		al_show_native_message_box(display, "Błąd!", "Nie udało się wczytać pliku audio Abeep.wav", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
	}
	if (SM_beep2 == NULL)
	{
		al_show_native_message_box(display, "Błąd!", "Nie udało się wczytać pliku audio Abeep2.wav", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
	}
	if (SM_beep3 == NULL)
	{
		al_show_native_message_box(display, "Błąd!", "Nie udało się wczytać pliku audio Abeep3.wav", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
	}
	if (FNT_48 == NULL)		return 1;

	return 0;          
}
short unload_all(void)
{
	destroy_resources();
	al_destroy_display(display);

	return 0;
}

short destroy_resources(void)
{
	if (BM_tlo != NULL)
		al_destroy_bitmap(BM_tlo);
	if (BM_kolko != NULL)
		al_destroy_bitmap(BM_kolko);
	if (BM_krzyz != NULL)
		al_destroy_bitmap(BM_krzyz);
	if (SM_beep != NULL)
		al_destroy_sample(SM_beep);
	if (SM_beep2 != NULL)
		al_destroy_sample(SM_beep2);
	if (SM_beep3 != NULL)
		al_destroy_sample(SM_beep3);
	if (SM_bg != NULL)
		al_destroy_sample(SM_bg);
	if (FNT_48 != NULL)
		al_destroy_font(FNT_48);
	if (FNT_20 != NULL)
		al_destroy_font(FNT_20);

	al_shutdown_ttf_addon();
    al_shutdown_font_addon();
	return 0;
}

short parse_mouse_input(int m_X, int mY)
{
	if ((m_X >= _X1) && (m_X <= _X2))
	{
		if ((mY >= _Y1) && (mY <= _Y2))
		{
			try_make_move(0,0);
			return 1;
		}
		else if ((mY >= _Y2) && (mY <= _Y3))
		{
			try_make_move(0,1);
			return 1;
		}
		else if ((mY >= _Y3) && (mY <= _Y4))
		{
			try_make_move(0,2);
			return 1;
		}
	}
	else if ((m_X >= _X2) && (m_X <= _X3))
	{
		if ((mY >= _Y1) && (mY <= _Y2))
		{
			try_make_move(1,0);
			return 1;
		}
		else if ((mY >= _Y2) && (mY <= _Y3))
		{
			try_make_move(1,1);
			return 1;
		}
		else if ((mY >= _Y3) && (mY <= _Y4))
		{
			try_make_move(1,2);
			return 1;
		}
	}
	else if ((m_X >= _X3) && (m_X <= _X4))
	{
		if ((mY >= _Y1) && (mY <= _Y2))
		{
			try_make_move(2,0);
			return 1;
		}
		else if ((mY >= _Y2) && (mY <= _Y3))
		{
			try_make_move(2,1);
			return 1;
		}
		else if ((mY >= _Y3) && (mY <= _Y4))
		{
			try_make_move(2,2);
			return 1;
		}
	}
	return 0;
}

short install_allegro(void)
{
	if(!al_init()) {
		fprintf(stderr, "failed to initialize allegro library!\n");
		return -1;
	}
	display = al_create_display(640, 480);
	if (!display)
	{
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}
	if (!al_install_keyboard()) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja klawiatury", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to initialize keyboard!\n");
		return -1;
	}
	if (!al_install_mouse()) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja myszki", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to initialize mouse!\n");
		return -1;
	}
	if (!al_init_image_addon()) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja image_addon()", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to initialize allegro imaging plugin!\n");
		return -1;
	}
	if (!al_install_audio()) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja systemu audio", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to initialize allegro audio plugin!\n");
		return -1;
	}
	if (!al_init_acodec_addon()) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja systemu kodeków audio", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to initialize allegro audio codec plugin!\n");
		return -1;
	}
	al_init_font_addon();
	if (!al_init_ttf_addon()) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja ttf_plugin", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to initialize allegro ttf font plugin!\n");
		return -1;
	}
	al_set_window_title(display, game_title);

	timer = al_create_timer(1.0 / 60.0);

	event_queue = al_create_event_queue();
	if(!event_queue) 
	{
		al_show_native_message_box(display, "Błąd!", "Nieudana inicjalizacja kolejki zdarzeń", "Gra się zamknie.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
		fprintf(stderr, "failed to create event queue!\n");
		return -1;
	}

	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_mouse_event_source());
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	return 0;
}


int main(int argc, char **argv)
{
	bool done = false;
	int pos_x = 0, pos_y = 0;
	int tx = 0, ty = 0;

	srand(time(NULL));
	prepare();

	if (install_allegro() != 0)
	{
		return -1;
	}

	if (load_resources())
	{
		fprintf(stderr, "failed to load resources!\n");
		return -1;
	}
	prepare_structures();

	al_start_timer(timer);

	al_play_sample(SM_bg, 1.0f, 1.0f, 1.0f, ALLEGRO_PLAYMODE_LOOP, &SM_bg_ID);

	while (!done)
	{
		al_wait_for_event(event_queue, &ev);

		if(ev.type == ALLEGRO_EVENT_TIMER)
		{
			al_clear_to_color(al_map_rgb(0,0,0));

			if (BM_tlo != NULL)
			{
				al_draw_bitmap(BM_tlo, 0.0f, 0.0f, 0);
			}

			for (tx = 0; tx < 3; tx++)
			{
				for (ty = 0; ty < 3; ty++)
				{
					if (get_state(tx,ty) == GRACZ_KOLKO)
					{
						al_draw_bitmap(BM_kolko, _X1 + tx * CONST_SPRITE_WIDTH, _Y1 + ty * CONST_SPRITE_HEIGHT, 0);
					}
					else if (get_state(tx,ty) == GRACZ_KRZYZ)
					{
						al_draw_bitmap(BM_krzyz, _X1 + tx * CONST_SPRITE_WIDTH, _Y1 + ty * CONST_SPRITE_HEIGHT, 0);
					}
				}
			}
			if (stan == STAN_PRZED)
			{
				al_draw_textf(FNT_48, al_map_rgb(0, 0, 0), 200, 30, ALLEGRO_ALIGN_LEFT, 
					( tura == GRACZ_KOLKO ? "Zaczyna kółko" : "Zaczyna krzyżyk"));
			}
			if (stan == STAN_GRAMY)
			{
				al_draw_textf(FNT_48, al_map_rgb(0, 0, 0), 200, 30, ALLEGRO_ALIGN_LEFT, 
					( tura == GRACZ_KOLKO ? "Tura kółka" : "Tura krzyżyka"));
			}
			else if (stan == STAN_KONIEC)
			{
				al_draw_textf(FNT_20, al_map_rgb(0, 0, 0), 150, 65, ALLEGRO_ALIGN_LEFT, "Prawy przycisk myszy restartuje grę");
				al_draw_textf(FNT_48, al_map_rgb(0, 0, 0), 200, 10, ALLEGRO_ALIGN_LEFT, "Koniec gry!");
				al_draw_textf(FNT_48, al_map_rgb(0, 0, 0), 200, 410, ALLEGRO_ALIGN_LEFT, (wygral==GRACZ_KOLKO?"Wygrało kółko":(wygral==GRACZ_KRZYZ?"Wygrał krzyżyk!":"Remis!!!")));
			}

			al_flip_display();
		}
		else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) 
		{
			done = true;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES)
		{
			pos_x = ev.mouse.x;
			pos_y = ev.mouse.y;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
		{
			if(ev.mouse.button & 1)
			{
				if (stan == STAN_GRAMY || stan == STAN_PRZED)
				{
					if (parse_mouse_input(pos_x, pos_y))
					{
						if (tura == GRACZ_KOLKO)
							do_beep(2);
						else 
							do_beep(1);
						if (stan == STAN_PRZED)
							stan = STAN_GRAMY;
						tx = check_who_won();
						if (tx != GRACZ_NIKT)
						{
							stan = STAN_KONIEC;
							wygral = tx;
						}
					}
					else
					{
						do_beep(3);
					}
				}
				else
				{
						do_beep(3);
				}
			}
			if (ev.mouse.button & 2)
			{
				if (stan == STAN_KONIEC)
				{
					do_beep(1);
					do_beep(2);

					prepare_structures();
				}
			}
		}
	}

	unload_all();

	return 0;
}